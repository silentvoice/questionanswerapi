addSbtPlugin("io.spray" % "sbt-revolver" % "0.7.2")

addSbtPlugin("name.heikoseeberger" % "sbt-groll" % "3.1.3")

addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.11.2")