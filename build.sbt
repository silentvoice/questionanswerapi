import spray.revolver.RevolverPlugin.Revolver

name := """ws-question-answers"""

version := "0.0.1"

organization := "Weird-Stats"

scalaVersion := "2.10.4"

resolvers ++= Seq(
  "typesafe.com" at "http://repo.typesafe.com/typesafe/repo/",
  "sonatype.org" at "https://oss.sonatype.org/content/repositories/releases",
  "spray.io" at "http://repo.spray.io"
)

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.3.2",
  "com.typesafe.akka" %% "akka-slf4j" % "2.3.2",
  "com.typesafe" % "config" % "1.2.0",
  "com.typesafe.scala-logging" %% "scala-logging-slf4j" % "2.1.2",
  "io.spray" % "spray-can" % "1.3.1",
  "io.spray" % "spray-routing" % "1.3.1",
  "io.spray" %% "spray-json" % "1.2.6",
  //  "io.spray"           %% "spray-client"       % "1.3.1",
  //  "org.json4s" %% "json4s-jackson" % "3.2.9",
  //  "org.json4s" %% "json4s-ext" % "3.2.9",
  //  "org.apache.logging.log4j" % "log4j-api" % "2.0-rc1",
  //  "org.apache.logging.log4j" % "log4j-core" % "2.0-rc1",
  //  "org.apache.logging.log4j" % "log4j-slf4j-impl" % "2.0-rc1",
  "ch.qos.logback" % "logback-classic" % "1.1.2"
)

scalacOptions ++= Seq(
  "-unchecked",
  "-deprecation",
  "-Xlint",
  "-Ywarn-dead-code",
  "-language:_",
  "-target:jvm-1.7",
  "-encoding", "UTF-8"
)

//testOptions += Tests.Argument(TestFrameworks.JUnit, "-v")

Revolver.settings: Seq[sbt.Def.Setting[_]]